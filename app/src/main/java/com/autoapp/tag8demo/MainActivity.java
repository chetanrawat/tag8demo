package com.autoapp.tag8demo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.autoapp.tag8demo.restapi.ApiRequests;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    TextInputLayout name, phone_no, email, address;
    Button btn_upload;
    TextView tv_upload,file_name;
    ProgressBar progress_bar;
    String filePath;
    Uri contentUri;
    private MultipartBody.Part file_body = null;
    private static final int REQUESTCODE_PICK_FILE = 120;
    private static final int REQUEST_STORAGE_PERMISSION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }


    private void initView() {
        name = findViewById(R.id.name);
        phone_no = findViewById(R.id.phone_no);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);
        btn_upload = findViewById(R.id.btn_upload);
        tv_upload = findViewById(R.id.tv_upload);
        progress_bar = findViewById(R.id.progress_bar);
        btn_upload.setOnClickListener(this);
        file_name = findViewById(R.id.file_name);
        tv_upload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_upload:
                validatePermission();
                break;
            case R.id.btn_upload:
                validateData(filePath);
                break;
            default:
                break;
        }

    }

    private void validatePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION
                    );
                }
            } else {
                openFileShare();
            }

        }
    }


    private void openFileShare() {
        Intent intent = new Intent();
        intent.setType("*/*");

        String[] mimetypes = {"application/pdf", "application/msword"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), REQUESTCODE_PICK_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFileShare();
                } else {
                    Toast.makeText(MainActivity.this, "permission denied !!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public String getImageFilePath(Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getApplicationContext().getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.d(TAG, "getImageFilePath: " + e.getMessage());
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) return;

        switch (requestCode) {
            case REQUESTCODE_PICK_FILE:
                contentUri = data.getData();
                filePath = getImageFilePath(contentUri);
                file_name.setVisibility(View.VISIBLE);
                file_name.setText(new File(filePath).getName());

        }
    }

    private void validateData(String filePath) {
        if (isInputValid()) {
            sendDataToServer(filePath);
        }else{
            Snackbar.make(tv_upload,"Fields Cannot be Empty.",Snackbar.LENGTH_LONG).show();
        }
    }

    private void sendDataToServer(String filePath) {
        if(filePath !=null){
            File file = new File(filePath);
            RequestBody body = RequestBody.create(MediaType.parse("multipart/form-file"), file);
            file_body = MultipartBody.Part.createFormData("file", file.getName(), body);

            Map<String, RequestBody> partMap = new HashMap<>();
            partMap.put("name", createPart(name.getEditText().getText().toString()));
            partMap.put("email", createPart(email.getEditText().getText().toString()));
            partMap.put("phone",createPart(phone_no.getEditText().getText().toString()));
            partMap.put("address", createPart(address.getEditText().getText().toString()));
            progress_bar.setVisibility(View.VISIBLE);

            ApiRequests apiRequests = new ApiRequests();

            apiRequests.uploadDetails(file_body, partMap, getApplicationContext());
            apiRequests.setUploadsListener(new ApiRequests.UploadsListener() {
                @Override
                public void onUpload(int code, String message) {
                    switch(code){
                        case 0:
                            progress_bar.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "Details submitted.", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            progress_bar.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "failed to submit details.", Toast.LENGTH_SHORT).show();
                            break;
                        case 10:
                            progress_bar.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "oops server error.", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });

        }else{
            Snackbar.make(btn_upload,"No file selected !",Snackbar.LENGTH_LONG).show();
        }

    }


    @NonNull
    private RequestBody createPart(String str){
        return RequestBody.create(MultipartBody.FORM, str);
    }







    private boolean isInputValid() {
        if (name.getEditText().getText().toString().trim().isEmpty()
                || phone_no.getEditText().getText().toString().trim().isEmpty()
                || email.getEditText().getText().toString().trim().isEmpty()
                || address.getEditText().getText().toString().trim().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}

