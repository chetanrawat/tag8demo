package com.autoapp.tag8demo.restapi;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiEndpoints {

    @Multipart
    @POST(ApiList.UPLOAD_CANDIDATE_DETAILS)
    Call<JsonObject> uploadDetails(@Part MultipartBody.Part image_body, @PartMap Map<String , RequestBody> data);
}
