package com.autoapp.tag8demo.restapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiRequests {


    private UploadsListener uploadsListener;

    public void uploadDetails(MultipartBody.Part body, Map<String, RequestBody> request, Context context) {
        if (checkConnection(context)) {
            ApiEndpoints endpoints = ApiClient.getAPI();
            Call<JsonObject> call = endpoints.uploadDetails(body, request);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject jsonObject = response.body();
                        if (jsonObject != null) {
                            if (jsonObject.has("response"))
                                uploadsListener.onUpload(jsonObject.get("code").getAsInt(), jsonObject.get("message").getAsString());
                            else
                                uploadsListener.onUpload(jsonObject.get("code").getAsInt(), jsonObject.get("message").getAsString());
                        } else
                            uploadsListener.onUpload(10, "Could not update profile. Please try again.");
                    } else
                        uploadsListener.onUpload(10, "Could not update profile. Please try again.");
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    uploadsListener.onUpload(10, "Could not update profile. Please try again.");
                }
            });
        } else Toast.makeText(context, "No Internet connection", Toast.LENGTH_SHORT).show();

    }


    public interface UploadsListener {
        void onUpload(int code, String message);
    }

    public void setUploadsListener(UploadsListener uploadsListener) {
        this.uploadsListener = uploadsListener;
    }

    public static boolean checkConnection(Context context) {
        boolean isConnected = false;

        ConnectivityManager conn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = conn.getActiveNetworkInfo();
        if (networkInfo != null) {
            isConnected = networkInfo.isConnected();
        }

        return isConnected;
    }
}
